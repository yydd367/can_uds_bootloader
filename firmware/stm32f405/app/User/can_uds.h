/**
  ******************************************************************************
  * @file    can_uds.h
  * @Author  wdluo
  * @brief   CAN UDS相关程序
  ******************************************************************************
  * @attention
  *Copyright 2018-2022, TOOMOSS
  *http://www.toomoss.com
  *All Rights Reserved
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __CAN_UDS_H
#define __CAN_UDS_H
/* Includes ------------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
#define DEBUG_SHOW_CAN_MSG    1 //显示原始CAN数据
#define DEBUG_SHOW_UDS_DATA   0 //显示原始UDS数据

#define UDS_PACK_SIZE   1024//建议为4字节整数倍
//单帧CAN最大字节数，普通CAN一般为8字节，CANFD最大可以设置为64字节
#define UDS_MAX_DLC     8   
//地址格式，0-normal, 1-extended ,2-mixed
enum ADDR_FORMATS
{
    NORMAL=0,EXTENDED,MIXED
};
#define UDS_ADDR_FORMATS    NORMAL
//UDS帧类型定义
enum UDS_PCI_TYPE
{
    UDS_SF=0,UDS_FF,UDS_CF,UDS_FC
};
//UDS FC定义
typedef  struct
{
    struct{
        uint8_t PCIType:4;
        uint8_t FC_FS:4;
    }PCIBits;
    uint8_t FC_BS;
    uint8_t FC_STmin;
}UDS_FC_HEAD;
//节点地址定义，上位机软件配置必须跟此处匹配，否则无法正常通信
#define PHY_ADDR_ID     0x700 //物理地址
#define FUN_ADDR_ID     0x7DF //功能地址
#define RSP_ADDR_ID     0x701 //响应地址
#define UDS_EXT_ADDR    0x01  //当UDS_ADDR_FORMATS不为NORMAL时，需要发送该数据
//定义数据收发帧ID类型,0-标准帧，1-扩展帧
#define	MSG_ID_TYPE		0
/* Functions -----------------------------------------------------------------*/
uint8_t CAN_UDS_SendData(uint8_t *pCANData,uint8_t DataLen);
void CAN_UDS_Process(uint8_t *pCANData,uint8_t DataLen);

#endif
