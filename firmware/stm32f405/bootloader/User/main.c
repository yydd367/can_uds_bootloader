/**
 * @file   main.c
 * @brief  CAN UDS Bootloader主函数
 * @author wdluo(wdluo@toomoss.com)
 * @version 1.0
 * @date 2022-10-18
 * @copyright Copyright (c) 2022 重庆图莫斯电子科技有限公司
 */
#include "main.h"
#include "usart.h"
#include "delay.h"
#include "can_boot.h"
#include "can_uds.h"
#include "can_buffer.h"
//CAN 消息FIFO变量
extern CAN_BUFFER  CANRxBuffer[2];

/**  
* @mainpage  CAN UDS Bootloader使用说明文档
* @section   内容简介
* 本文档为CAN UDS Bootloader源码说明文档，参考此文档可以对程序框架有个较为清晰的认识，对程序移植有一定的帮助。
* 
* @section   用法描述 
* -# 本程序目前只实现了CAN UDS固件升级命令和数据传输功能，数据写入芯片Flash，程序跳转功能还未实现
* -# UDS相关函数可以直接复制使用，不需要做太多的修改，移植到不同单片机，主要移植CAN驱动程序和BOOT相关程序即可
* -# 从APP进入BOOT需要使能进入编程模式标志，该标志在程序复位或者重新上电后不能丢失
* -# 目前程序可以直接在STM32F4系列单片机上直接运行，其他单片机运行还需要进一步移植
*
* @section   版本记录 
* <table>
* <tr><th>日期        <th>版本   <th>作者    <th>描述</tr>
* <tr><td>2022/10/18 <td>1.0    <td>wdluo  <td>创建初始版本 </tr>
* </table>
*<center>Copyright (c) 2022 重庆图莫斯电子科技有限公司</center>
**********************************************************************************
*/

/**
 * @brief  主函数
 * @return 程序内部为死循环，返回值无意义
 */
int main(void)
{
  /* At this stage the microcontroller clock setting is already configured, 
  this is done through SystemInit() function which is called from startup
  file (startup_stm32fxxx_xx.s) before to branch to application main.
  To reconfigure the default setting of SystemInit() function, refer to
  system_stm32fxxx.c file
  */
  __set_PRIMASK(0);//开启总中断，若程序是在APP中跳转过来的，总中断有可能被关闭，所以这里需要打开
  delay_init(168);
  USART_Configuration();//配置串口，用于打印调试信息
  printf("---------------------------------------------------------------------\r\n");
  printf("Start CAN UDS Bootloader\r\n");
  if(!CAN_BOOT_GetProgRequest()){
    if(*((uint32_t *)APP_VALID_FLAG_ADDR)==APP_VALID_FLAG){
      printf("Exe App...\r\n");
      delay_ms(10);
      CAN_BOOT_ExeApp();
    }
  }
  printf("Exe Boot...\r\n");
  CAN_BUF_Init(0);
  CAN_Configuration(0,500000);
  printf("CAN1 Config End\r\n");
  printf("APP_VALID_FLAG = 0x%08X\r\n",*((uint32_t *)APP_VALID_FLAG_ADDR));
  printf("BOOT_REQ_FLAG = 0x%08X\r\n",*((uint32_t *)BOOT_REQ_FLAG_ADDR));
  while (1)
  {
    CAN_MSG *pMsg;
    if(CAN_BUF_PopMsg(0,&pMsg,1)){
#if DEBUG_SHOW_CAN_MSG
      printf("%s CAN(0x%08X):",((pMsg->Flags.DIR)?"TX":"RX"),pMsg->ID);
      for(int i=0;i<pMsg->Flags.DLC;i++){
        printf("%02X ",pMsg->Data[i]);
      }
      printf("\r\n");
#endif
      if(pMsg->Flags.DIR){
        CAN_WriteData(0,pMsg);
      }else{
        CAN_UDS_Process(pMsg->Data,pMsg->Flags.DLC);
      }
    }
  }
}
